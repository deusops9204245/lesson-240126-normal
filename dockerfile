FROM node:16-alpine AS build

WORKDIR /app

COPY package*.json ./

COPY webpack.config.js ./

COPY . .

RUN npm install --include=dev

RUN npm run build

FROM nginx:alpine

WORKDIR /app

COPY --from=build /app/package*.json ./
COPY --from=build /app/dist ./dist
COPY --from=build /app/webpack.config.js ./
COPY --from=build /app/src ./src
COPY --from=build /app/dist/index.html /usr/share/nginx/html/
COPY --from=build /app/src/index.js /usr/share/nginx/html/
COPY --from=build /app/dist/lib/bundle.js /usr/share/nginx/html/lib/

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]